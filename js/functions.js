jQuery(document).ready(function ($) {

    $('.owl-carousel').owlCarousel({
    	items: 1,
        loop: true,
        nav: true,
        dots: false,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
    });

    $('select').styler({
        locale: 'en',
            locales: {
            'en': {
              filePlaceholder: 'No file selected',
              fileBrowse: 'Browse...',
              fileNumber: 'Selected files: %s',
              selectPlaceholder: 'Select...',
              selectSearchNotFound: 'No matches found',
              selectSearchPlaceholder: 'Search...'
            }
        }
    });

    //fixed header
    if($(window).width() >= 1024) {
        var HeaderHeight = $('.header').outerHeight();
        $(window).on('scroll', function() {
            if ($(this).scrollTop() >= HeaderHeight) {
                $('.header').addClass('fixed-header');
            } else {
                $('.header').removeClass('fixed-header');
            };
        });
    };

    $('#menu-toggle').on('click', function(){
        if ($('.navbar-collapse').css('display') == 'block') {
            $('.navbar-collapse').fadeOut('slow');
            $('#menu-toggle').removeClass('open');
        } else {
            $('.navbar-collapse').fadeIn('slow');
            $('#menu-toggle').addClass('open');
        }
    });

    $(window).resize(function () {
        if ($(window).width() >= 1024) {
            if ($('.navbar-collapse').css('display') == 'none') {
                $('.navbar-collapse').css('display', 'block');
            }
        } else {
            if ($('.navbar-collapse').css('display') == 'block') {
                $('.navbar-collapse').css('display', 'none');
                $('#menu-toggle').removeClass('open');
            }
        }
    });

    $( "#tabs" ).tabs();

});
